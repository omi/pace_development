import earthaccess
import random
import h5py
import gc 
import numpy as np 
from scipy import stats
import glob 

#Simple function for finding nearest index in array for specific value
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


#Simple function for nearest neighbor interpolation to regular grid 
def colocate_data(grid,lon,lat,lon_bins,lat_bins):

    lon_mid = (lon_bins[0:-1]+lon_bins[1:])/2.
    lat_mid = (lat_bins[0:-1]+lat_bins[1:])/2.

    lon_inds = np.array([find_nearest(lon_mid,lon[i]) for  i in range(len(lon))])
    lat_inds = np.array([find_nearest(lat_mid,lat[i]) for  i in range(len(lat))])

    return grid[lon_inds,lat_inds]


#Function for colocating TROPOMI NO2 data to PACE FoV
#Requires the following inputs:
#pace_lon - 1d array of PACE longitude values
#pace_lat - 1d array of PACE latitude values
#start_date - string representing start time in format 'YYY-MM-DD HH:MM:SS'
#end_date - string representing end time in format 'YYY-MM-DD HH:MM:SS'
#Optional Input (that normally should be passed in...):
#file_path(optional) - location for TROPOMI data files to be downloaded...works better to download TROPOMI data rather than pulling into memory each time
def colocate_tropomi(pace_lon,pace_lat,start_date,end_date,file_path = '/explore/nobackup/people/zfasnach/PACE_Data/'):

    year = int(start_date[0:4])
    month = int(start_date[5:7])
    day = int(start_date[8:10])
    

    #Checking if TROPOMI NO2 files have already been downloaded, if not, they are downloaded from earthdata
    #Note count = 20 assumes the colocation is being run for a single day (approx 15 orbits per day)
    #Note the connection normally drops once or twice, so might need to run again to catch all files, if run again, it only downloads files that didn't download yet
    trop_no2_files = glob.glob(file_path+'/S5P_OFFL_L2__NO2____'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*')
    if len(trop_no2_files) == 0:
        earthaccess.login(persist=True)    
        results = earthaccess.search_data(short_name = 'S5P_L2__NO2____HiR',cloud_hosted=True,temporal=(start_date,end_date),count=20,bounding_box=(-180,-90,180,90))
        trop_no2_files = earthaccess.open(results)
        earthaccess.download(results,file_path)


    #Selected TROPOMI variables to colocate to PACE FoV 
    tropomi_keys = ['no2_scd','no2_vcd','amf_strat','no2_strat','spress','amf_trop']

    data_sums = {}
    data_counts = {}
    l2_data = {}
    pace_colocated = {}
    gridded_data = {}
    
    lon_bins = np.arange(-180,180.05,0.05)
    lat_bins = np.arange(-90,90.05,0.05)

    for key in tropomi_keys:
        data_sums[key] = np.zeros((len(lon_bins)-1,len(lat_bins)-1))
        data_counts[key] = np.zeros((len(lon_bins)-1,len(lat_bins)-1))

        
    for filename in trop_no2_files:
        print(filename)

        #Reading TROPOMI L2 NO2 files
        f = h5py.File(filename,'r')  
        
        data_group = '/PRODUCT/SUPPORT_DATA/DETAILED_RESULTS/'
        product_group = '/PRODUCT/'
        vza = f['/PRODUCT/SUPPORT_DATA/GEOLOCATIONS/viewing_zenith_angle'][0]
        sza = f['/PRODUCT/SUPPORT_DATA/GEOLOCATIONS/solar_zenith_angle'][0]

        l2_data['no2_strat'] = f[data_group+'nitrogendioxide_stratospheric_column'][0]
        l2_data['no2_strat'][l2_data['no2_strat'] == 9.96921e+36] = np.nan

        l2_data['amf_strat'] = f[data_group+'air_mass_factor_stratosphere'][0]
        l2_data['amf_strat'][l2_data['amf_strat'] == 9.96921e+36] = np.nan

        l2_data['amf_trop'] = f[product_group+'air_mass_factor_troposphere'][0]
        l2_data['amf_trop'][l2_data['amf_trop'] == 9.96921e+36] = np.nan

        l2_data['spress'] = np.log(f['/PRODUCT/SUPPORT_DATA/INPUT_DATA/surface_pressure'][0])
        l2_data['spress'][l2_data['spress'] ==  9.96921e+36] = np.nan

        no2_trop = f[product_group+'nitrogendioxide_tropospheric_column'][0]
        no2_trop[no2_trop == 9.96921e+36] = np.nan
        l2_data['no2_vcd'] = no2_trop + l2_data['no2_strat']

        no2_scd = f[data_group+'nitrogendioxide_slant_column_density'][0]
        no2_scd_stripe = f[data_group+'nitrogendioxide_slant_column_density_stripe_amplitude'][0]

        #Calculating TROPOMI SCD by applying stripe correction and dividing by Geometric AMF to remove angular dependence of SCD 
        amf = (1./np.cos(np.radians(sza))) + (1./np.cos(np.radians(vza)))
        nline, nrow = no2_scd.shape
        
        no2_scd_stripe[no2_scd_stripe == 9.96921e+36] = np.nan
        no2_scd = no2_scd -  np.repeat(no2_scd_stripe[np.newaxis,:],nline,axis=0)
            
        no2_scd[no2_scd == 9.96921e+36] = np.nan
        l2_data['no2_scd'] = no2_scd/amf
        
        trop_lat = f[product_group+'latitude'][0]
        trop_lon = f[product_group+'longitude'][0]

        qc = f['/PRODUCT/qa_value'][0]

        #Screening TROPOMI data to grab only highest quality TROPOMI data
        nan_inds = (~np.isnan(l2_data['no2_scd'])) & (l2_data['no2_strat'] != 9.96921e+36) & (qc > 75)

        #Binning Level 2 TROPOMI data to 0.05x0.05 degree grid which will then be used to co-locate to PACE FoV
        for key in tropomi_keys:

            ret_sum, x_edge, y_edge, _ = stats.binned_statistic_2d(trop_lon[nan_inds], trop_lat[nan_inds], l2_data[key][nan_inds], 'sum', bins=[lon_bins,lat_bins])
            ret_count, x_edge, y_edge, _  = stats.binned_statistic_2d(trop_lon[nan_inds], trop_lat[nan_inds], l2_data[key][nan_inds], 'count', bins=[lon_bins,lat_bins])
        
            inds = ret_count > 0
            data_sums[key][inds] += ret_sum[inds]
            data_counts[key][inds] += ret_count[inds]

        f.close()

    #Calculating gridded TROPOMI 0.05x0.05 degree gridded data after summing up each individual orbit 
    for key in tropomi_keys:
        gridded_data[key] = data_sums[key]/data_counts[key]
        gridded_data[key][gridded_data[key] == 0] = np.nan

        
        if key == 'spress':
            gridded_data[key] = np.exp(gridded_data[key])

    #Colocating TROPOMI data to PACE FoV using simple lat/lon index search since a regular 2d grid is used for TROPOMI
    for key in tropomi_keys:
        pace_colocated[key] = colocate_data(gridded_data[key],pace_lon,pace_lat,lon_bins,lat_bins)

    return pace_colocated


#Function for grabbing PACE L1c 5km files
#Requires the following inputs:
#start_date - string representing start time in format 'YYY-MM-DD HH:MM:SS'
#end_date - string representing end time in format 'YYY-MM-DD HH:MM:SS'

#Optional Input:
#min_lon (default = -180), max_lon (default = 180), min_lat (default = -90), max_lat (default =90) - Lat/Lon bounding box for requested PACE files. 
#file_path(optional) - location for TROPOMI data files to be downloaded...works better to download TROPOMI data rather than pulling into memory each time
#split_pace_data (default = 1.0) - since PACE files are quite large, this allows user to select a random fraction of the PACE data (0-1.0) to be returned to save memory, data are shuffled before
#making selection to make sure that the selection is random
#min_wave (default = 360), max_wave (default = 900) - minimum and maximimum wavelength user wants radiances returned 

def grab_pace_l1c(start_date,end_date,min_lon=-180,max_lon=180,min_lat=-90,max_lat=90,file_path = '/explore/nobackup/people/zfasnach/PACE_Data/',split_pace_data=1.,min_wave=360,max_wave=1000):

    year = int(start_date[0:4])
    month = int(start_date[5:7])
    day = int(start_date[8:10])


    #Checking if PACE L1c files have been downloaded and downloading them to file_path if they don't exist
    #Note the connection normally drops once or twice, so might need to run again to catch all files, if run again, it only downloads files that didn't download yet
    pace_files = glob.glob(file_path+'PACE_OCI*'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*.L1C.V2.5km.nc')
    if len(pace_files) == 0:
        earthaccess.login(persist=True)
        results = earthaccess.search_data(short_name = 'PACE_OCI_L1C_SCI',cloud_hosted=True,temporal=(start_date,end_date),count=200,bounding_box=(min_lon,min_lat,max_lon,max_lat),version='2')
        pace_files = earthaccess.open(results)
        earthaccess.download(results,file_path)
        pace_files = glob.glob(file_path+'PACE_OCI*'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*.L1C.V2.5km.nc')
        
    start = True

    pace_data = {}


    for filename in pace_files:
        print(filename)
        f = h5py.File(filename,'r')

        pace_lat = np.array(f['/geolocation_data/latitude'][()])
        pace_lon = np.array(f['/geolocation_data/longitude'][()])

        #Reading what seems to be a good proxy for tilt angle, >0 means tilt up, <0 means tilt down 
        pace_hem = np.array(f['/bin_attributes/view_time_offsets'][:])
        pace_hem[pace_hem == -32767] = np.nan

        #Skipping PACE file if it doesn't fall within requested lat/lon boundaries 
        geo_inds = (pace_lat > min_lat) & (pace_lat < max_lat) & (pace_lon > min_lon) & (pace_lon < max_lon)
        if len(pace_lat[geo_inds]) == 0:
            continue
        print(f.keys())

        #L1c reader 
        radiance = f['/observation_data/i'][()]
        irr = f['/sensor_views_bands/intensity_f0'][()]
        wavelength = f['/sensor_views_bands/intensity_wavelength'][0]


        #Selecting radiances for requested wavelength range, setting bad radiances to NaN, and calculating normalized reflectances a
        wave_ind = (wavelength > min_wave) & (wavelength < max_wave)

        rad_qc = f['/observation_data/qc'][()]

        radiance[(radiance == -32767) | (rad_qc > 0)] = np.nan
    
        nline, nrow, nview, nwave = radiance.shape

        irr = np.repeat(np.repeat(irr[np.newaxis,:,:],nrow,axis=0)[np.newaxis,:,:,:],nline,axis=0)

        inds = ~np.isnan(np.nanmean(radiance,axis=-1))
        normalized_radiance = np.array(radiance[inds,:]/irr[inds,:])[:,wave_ind]
        
        del radiance, irr
        gc.collect()
        
        #L1c reader for geo data
        pace_vza = np.array(f['/geolocation_data/sensor_zenith_angle'][()],dtype=np.float32)[inds]
        pace_vza[pace_vza == -32767] = np.nan
	pace_vza = pace_vza * 0.01
        pace_sza = np.array(f['/geolocation_data/solar_zenith_angle'][()],dtype=np.float32)[inds]
        pace_sza[pace_sza == -32767] = np.nan
        pace_sza = pace_sza * 0.01
        pace_saa = np.array(f['/geolocation_data/solar_azimuth_angle'][()],dtype=np.float32)[inds]
	pace_saa[pace_saa == -32767] = np.nan
        pace_saa = pace_saa * 0.01
        pace_vaa = np.array(f['/geolocation_data/sensor_azimuth_angle'][()],dtype=np.float32)[inds]
        pace_vaa[pace_vaa == -32767] = np.nan
        pace_vaa = pace_vaa * 0.01
        
        
        pace_raa = np.array(pace_vaa)-np.array(pace_saa)
        pace_raa[pace_raa > 360] = pace_raa[pace_raa > 360] - 360
        pace_raa[pace_raa < 0] = pace_raa[pace_raa < 0] + 360
    
    
        pace_lat = np.repeat(np.array(f['/geolocation_data/latitude'][()])[:,:,np.newaxis],2,axis=-1)[inds]
        pace_lon = np.repeat(np.array(f['/geolocation_data/longitude'][()])[:,:,np.newaxis],2,axis=-1)[inds]

        pace_hem = pace_hem[inds]
        f.close()

            
        geo_inds = (pace_lat > min_lat) & (pace_lat < max_lat) & (pace_lon > min_lon) & (pace_lon < max_lon) 
        inds =  geo_inds & (~np.isnan(pace_hem))

        #Selecting random amount of data to return based on user defined fraction "split_pace_data"
        perc_inds = np.arange(len(pace_vza))[inds]
        random.shuffle(perc_inds)
        perc_inds = perc_inds[0:int(len(perc_inds)*split_pace_data)]

                        
        if start:
            pace_data['sza'] = np.array(pace_sza)[perc_inds]
            pace_data['hemisphere'] = np.array(pace_hem)[perc_inds]
            pace_data['raa'] = np.array(pace_raa)[perc_inds]
            pace_data['vza'] = np.array(pace_vza)[perc_inds]
            pace_data['lat'] = np.array(pace_lat)[perc_inds]
            pace_data['lon'] = np.array(pace_lon)[perc_inds]
            pace_data['rad'] = np.array(normalized_radiance)[perc_inds,:]
            pace_data['wavelength'] = np.array(wavelength[wave_ind])
            start = False
        else:            
            pace_data['sza'] = np.hstack((pace_data['sza'],pace_sza[perc_inds]))
            pace_data['hemisphere'] = np.hstack((pace_data['hemisphere'],pace_hem[perc_inds]))            
            pace_data['raa'] = np.hstack((pace_data['raa'],pace_raa[perc_inds]))
            pace_data['vza'] = np.hstack((pace_data['vza'],pace_vza[perc_inds]))
            pace_data['lat'] = np.hstack((pace_data['lat'],pace_lat[perc_inds]))
            pace_data['lon'] = np.hstack((pace_data['lon'],pace_lon[perc_inds]))
            pace_data['rad'] = np.vstack((pace_data['rad'],normalized_radiance[perc_inds,:]))


    return pace_data

#Function for grabbing PACE L1b 1km files
#Requires the following inputs:
#start_date - string representing start time in format 'YYY-MM-DD HH:MM:SS'
#end_date - string representing end time in format 'YYY-MM-DD HH:MM:SS'

#Optional Input:
#min_lon (default = -180), max_lon (default = 180), min_lat (default = -90), max_lat (default =90) - Lat/Lon bounding box for requested PACE files. 
#file_path(optional) - location for TROPOMI data files to be downloaded...works better to download TROPOMI data rather than pulling into memory each time
#split_pace_data (default = 1.0) - since PACE files are quite large, this allows user to select a random fraction of the PACE data (0-1.0) to be returned to save memory, data are shuffled before
#making selection to make sure that the selection is random
#min_wave (default = 360), max_wave (default = 900) - minimum and maximimum wavelength user wants radiances returned 

def grab_pace_l1b(start_date,end_date,min_lon=-180,max_lon=180,min_lat=-90,max_lat=90,file_path = '/explore/nobackup/people/zfasnach/PACE_Data/',split_pace_data=1.,min_wave=360,max_wave=1000):

    year = int(start_date[0:4])
    month = int(start_date[5:7])
    day = int(start_date[8:10])

    
    #Checking if PACE L1c files have been downloaded and downloading them to file_path if they don't exist
    #Note the connection normally drops once or twice, so might need to run again to catch all files, if run again, it only downloads files that didn't download yet
    pace_files = glob.glob(file_path+'PACE_OCI*'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*.L1B.V2.nc')
    if len(pace_files) == 0:
        earthaccess.login(persist=True)
        results = earthaccess.search_data(short_name = 'PACE_OCI_L1B_SCI',cloud_hosted=True,temporal=(start_date,end_date),count=200,bounding_box=(min_lon,min_lat,max_lon,max_lat),version='2')
        pace_files = earthaccess.open(results)
        earthaccess.download(results,file_path)
        pace_files = glob.glob(file_path+'PACE_OCI*'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*.L1B.V2.nc')

        
    start = True

    pace_data = {}


    for filename in pace_files:
        print(filename)
        f = h5py.File(filename,'r')

        pace_lat = np.array(f['/geolocation_data/latitude'][()])
        pace_lon = np.array(f['/geolocation_data/longitude'][()])

        #Storing tilt angle information. Since there are 2 tilt angles, it seems logical to train 2 NN models, one for each tilt angle 
        pace_hem = np.array(f['/navigation_data/tilt_angle'][:])

        pace_hem[pace_hem == -32767] = np.nan

        geo_inds = (pace_lat > min_lat) & (pace_lat < max_lat) & (pace_lon > min_lon) & (pace_lon < max_lon)
        if len(pace_lat[geo_inds]) == 0:
            continue
        print(f.keys())

        #L1b reader
        radiance = np.vstack((f['/observation_data/rhot_blue'][()],f['/observation_data/rhot_red'][()],f['/observation_data/rhot_SWIR'][()]))
        wavelength = np.hstack((f['/sensor_band_parameters/blue_wavelength'][()],f['/sensor_band_parameters/red_wavelength'][()],f['/sensor_band_parameters/SWIR_wavelength'][()]))

        wave_ind = (wavelength > min_wave) & (wavelength < max_wave)

        rad_qc = np.vstack((f['/observation_data/qual_blue'][()],f['/observation_data/qual_red'][()],f['/observation_data/qual_SWIR'][()]))

        
        radiance[(radiance == -32767) | (rad_qc > 0)] = np.nan

    
        nwave, nline, nrow = radiance.shape
        inds = ~np.isnan(np.nanmean(radiance,axis=0))
        radiance = np.swapaxes(np.array(radiance[:,inds])[wave_ind,:],0,1)

        pace_hem = np.repeat(pace_hem[:,np.newaxis],nrow,axis=1)
        
        #Grabbing PACE chlorophyll data for current granule 
        pace_chl, pace_l2flags = grab_pace_chl(start_date,end_date,filename.replace('.L1B.V2.nc',''))


        pace_chl[pace_chl == -32767] = np.nan
        
        #L1b reader of geolocation data 
        pace_vza = np.array(f['/geolocation_data/sensor_zenith'][()],dtype=np.float32)[inds]
        pace_vza[pace_vza == -32767] = np.nan
	pace_vza = pace_vza * 0.01
        pace_sza = np.array(f['/geolocation_data/solar_zenith'][()],dtype=np.float32)[inds]
        pace_sza[pace_sza == -32767] = np.nan
        pace_sza = pace_sza * 0.01
        pace_saa = np.array(f['/geolocation_data/solar_azimuth'][()],dtype=np.float32)[inds]
	    pace_saa[pace_saa == -32767] = np.nan
        pace_saa = pace_saa * 0.01
        pace_vaa = np.array(f['/geolocation_data/sensor_azimuth'][()],dtype=np.float32)[inds]
        pace_vaa[pace_vaa == -32767] = np.nan
        pace_vaa = pace_vaa * 0.01

        pace_raa = np.array(pace_vaa)-np.array(pace_saa)
        pace_raa[pace_raa > 360] = pace_raa[pace_raa > 360] - 360
        pace_raa[pace_raa < 0] = pace_raa[pace_raa < 0] + 360

        pace_lat = pace_lat[inds]
        pace_lon = pace_lon[inds]
        pace_hem = pace_hem[inds]
        f.close()

        pace_chl = pace_chl[inds]
        pace_l2flags = np.array(pace_l2flags[inds],dtype=int)

        #Can screen PACE data based on L2 flags, in this case since we are looking at gap filling, we want to save all data 
        #flagged_bits = [0,1,3,4,5,8,9,10,12,14,15,16,19,21,22,25]
        #for bit in flagged_bits:
        #    pace_chl[pace_l2flags & (2**bit) == 2**bit] = -32767
        

        #Only storing PACE data for selected lat/lon boundaries, pace_l2flags = 2 is land, current application is for ocean color, so only storing data over water 
        geo_inds = (pace_lat > min_lat) & (pace_lat < max_lat) & (pace_lon > min_lon) & (pace_lon < max_lon) 
        inds =  geo_inds & (~np.isnan(pace_hem)) &  (pace_l2flags & 2 != 2)

        #Selecting random amount of data to return based on user defined fraction "split_pace_data"
        perc_inds = np.arange(len(pace_vza))[inds]
        random.shuffle(perc_inds)
        perc_inds = perc_inds[0:int(len(perc_inds)*split_pace_data)]
                        
        if start:
            pace_data['sza'] = np.array(pace_sza)[perc_inds]
            pace_data['chl'] = np.array(pace_chl)[perc_inds]
            pace_data['l2flags'] = np.array(pace_l2flags)[perc_inds]
            pace_data['hemisphere'] = np.array(pace_hem)[perc_inds]
            pace_data['raa'] = np.array(pace_raa)[perc_inds]
            pace_data['vza'] = np.array(pace_vza)[perc_inds]
            pace_data['lat'] = np.array(pace_lat)[perc_inds]
            pace_data['lon'] = np.array(pace_lon)[perc_inds]
            pace_data['rad'] = np.array(radiance)[perc_inds,:]
            pace_data['wavelength'] = np.array(wavelength[wave_ind])
            start = False
        else:            
            pace_data['sza'] = np.hstack((pace_data['sza'],pace_sza[perc_inds]))
            pace_data['chl'] = np.hstack((pace_data['chl'],pace_chl[perc_inds]))
            pace_data['l2flags'] = np.hstack((pace_data['l2flags'],pace_l2flags[perc_inds]))
            pace_data['hemisphere'] = np.hstack((pace_data['hemisphere'],pace_hem[perc_inds]))            
            pace_data['raa'] = np.hstack((pace_data['raa'],pace_raa[perc_inds]))
            pace_data['vza'] = np.hstack((pace_data['vza'],pace_vza[perc_inds]))
            pace_data['lat'] = np.hstack((pace_data['lat'],pace_lat[perc_inds]))
            pace_data['lon'] = np.hstack((pace_data['lon'],pace_lon[perc_inds]))
            pace_data['rad'] = np.vstack((pace_data['rad'],radiance[perc_inds,:]))


    return pace_data


#Function for grabbing PACE L2 chlorophyll data 
#Requires the following inputs:
#start_date - string representing start time in format 'YYY-MM-DD HH:MM:SS'
#end_date - string representing end time in format 'YYY-MM-DD HH:MM:SS'
#fname-prefix - time stamp for requested L2 chlorophyll data based on given L1b filename 

#Optional Input:
#min_lon (default = -180), max_lon (default = 180), min_lat (default = -90), max_lat (default =90) - Lat/Lon bounding box for requested PACE files. 
#file_path(optional) - location for TROPOMI data files to be downloaded...works better to download TROPOMI data rather than pulling into memory each time
#making selection to make sure that the selection is random
#min_wave (default = 360), max_wave (default = 900) - minimum and maximimum wavelength user wants radiances returned 

def grab_pace_chl(start_date,end_date,fname_prefix,min_lon=-180,max_lon=180,min_lat=-90,max_lat=90,file_path = '/explore/nobackup/people/zfasnach/PACE_Data/'):
    year = int(start_date[0:4])
    month = int(start_date[5:7])
    day = int(start_date[8:10])

    #Checking to see if requested L2 chlorophyll file has been downloaded. If it is not found, then it downloads the chlorophyll files for current day and searches again
    pace_files = glob.glob(fname_prefix+'.L2.OC_BGC.V2_0.NRT.nc')

    if len(pace_files) == 0:
        earthaccess.login(persist=True)
        results = earthaccess.search_data(short_name = 'PACE_OCI_L2_BGC_NRT',cloud_hosted=True,temporal=(start_date,end_date),count=200,bounding_box=(min_lon,min_lat,max_lon,max_lat),version='2.0')
        pace_files = earthaccess.open(results)
        earthaccess.download(results,file_path)

        pace_files = glob.glob(fname_prefix+'.L2.OC_BGC.V2_0.NRT.nc')

    #Reading/returning PACE L2 chlorophyll and flags 
    f = h5py.File(pace_files[0],'r')
    chl =  f['/geophysical_data/chlor_a'][()]
    l2_flags = f['/geophysical_data/l2_flags'][()]
    f.close()

    return chl, l2_flags


    
